<?php

namespace Enjoying\Core\DependencyInjection;

use Enjoying\Core\DependencyInjection\ServiceDefinition;

interface IDependencyContainer
{
    function getServiceDefinition($serviceName);
    function registerService($serviceName, ServiceDefinition $service);
    function getService($serviceName, $newInstance = false);
}