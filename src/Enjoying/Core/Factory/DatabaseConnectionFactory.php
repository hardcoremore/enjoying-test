<?php

namespace Enjoying\Core\Factory;

use Enjoying\Core\Database\DatabaseConnectionException;

class DatabaseConnectionFactory
{
    public function createPDO(array $config)
    {
        try
        {
            $conn = new \PDO(
                'mysql:host=' . $config['host'] . ';dbname=' . $config['database'] . ';port=' . $config['port'] . ';charset=' . $config['charset'],
                $config['username'],
                $config['password']
            );

            $conn->setAttribute(\PDO::ATTR_EMULATE_PREPARES, false);
            return $conn;
        }
        catch(\PDOException $exception)
        {
            throw new DatabaseConnectionException('Could not connect to database');
        }
    }
}