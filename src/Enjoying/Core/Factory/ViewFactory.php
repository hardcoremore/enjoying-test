<?php

namespace Enjoying\Core\Factory;

use Enjoying\Core\Factory\BaseCamelCaseFactory;

use Enjoying\Core\View\JsonView;
use Enjoying\Core\View\HtmlView;

class ViewFactory extends BaseCamelCaseFactory
{
    public function __construct()
    {
        $this->methodPostfix = 'View';
    }

    public function createJsonView()
    {
        return new JsonView();
    }

    public function createHtmlView()
    {
        return new HtmlView();
    }
}