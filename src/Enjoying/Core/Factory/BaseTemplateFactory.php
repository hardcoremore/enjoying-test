<?php

namespace Enjoying\Core\Factory;

use Enjoying\Core\Factory\BaseCamelCaseFactory;

use Enjoying\Core\Template\BaseHtmlTemplate;
use Enjoying\Core\Template\FormTemplate;
use Enjoying\Core\Template\FormControlsTemplate;
use Enjoying\Core\Template\TextInputFormItemTemplate;
use Enjoying\Core\Template\TextAreaInputFormItemTemplate;

class BaseTemplateFactory extends BaseCamelCaseFactory
{
    public function __construct()
    {
        $this->methodPostfix = 'Template';
    }

    public function createHtmlTemplate()
    {
        return new BaseHtmlTemplate();
    }

    public function createFormTemplate()
    {
        return new FormTemplate($this);
    }

    public function createFormControlsTemplate()
    {
        return new FormControlsTemplate($this);
    }

    public function createTextInputFormItemTemplate()
    {
        return new TextInputFormItemTemplate($this);
    }

    public function createTextAreaInputFormItemTemplate()
    {
        return new TextAreaInputFormItemTemplate($this);
    }
}