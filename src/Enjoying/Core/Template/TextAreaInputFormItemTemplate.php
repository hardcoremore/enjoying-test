<?php

namespace Enjoying\Core\Template;

use Enjoying\Core\Template;

class TextAreaInputFormItemTemplate extends BaseHtmlTemplate
{
    private $template = <<<EOF
<div class="form-field-set {{class}}">
    <span class="form-field-error form-field-error-{{name}}" data-field-name="{{name}}"></span>
    <label class="form-field-label" for="{{name}}-{{nameSpace}}-input">{{label}}</label>
    <textarea id="{{name}}-{{nameSpace}}-input" name="{{ name }}" class="form-field-input"></textarea>
</div>


EOF;

    public function getTemplate()
    {
       return $this->template;
    }

    public function comile()
    {
        $this->setProperty('class', 'text-area');
        
        parent::compile();

        return $this;
    }
}
