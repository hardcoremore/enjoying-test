<?php

namespace Enjoying\Core\Template;

use Enjoying\Core\Template;

class TextInputFormItemTemplate extends BaseHtmlTemplate
{
    private $template = <<<EOF
<div class="form-field-set {{class}}">
    <span class="form-field-error form-field-error-{{name}}" data-field-name="{{name}}"></span>
    <label class="form-field-label" for="{{name}}-{{nameSpace}}-input">{{label}}</label>
    <input type="{{type}}" class="form-field-input" name="{{name}}" id="{{name}}-{{nameSpace}}-input" placeholder="{{placeHolder}}" />
</div>
EOF;

    public function getTemplate()
    {
        return $this->template;
    }

    public function comile()
    {
        $this->setProperty('class', 'text-input');
        
        parent::compile();

        return $this;
    }
}