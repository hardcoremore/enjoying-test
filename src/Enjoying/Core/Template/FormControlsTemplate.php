<?php

namespace Enjoying\Core\Template;

use Enjoying\Core\Template;

class FormControlsTemplate extends BaseHtmlTemplate
{

    private $template = <<<EOF

<div class="form-field-set form-controls-set {{formControlsClass}}">

    <div class="form-controls">

        <input type="submit" value="{{saveFormButtonLabel}}">

        <a href="{{cancelFormButtonUrl}}">{{cancelFormButtonLabel}}</a>

    </div>

</div>

EOF;

    public function getTemplate()
    {
        return $this->template;
    }
}