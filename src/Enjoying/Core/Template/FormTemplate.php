<?php

namespace Enjoying\Core\Template;

use Enjoying\Core\Template;

class FormTemplate extends BaseHtmlTemplate
{
    private $template = <<<EOF
<form method="post" action="{{actionUrl}}" id="{{formId}}" class="crud-module-form {{formClass}}">

    {%formInputs%}

    {%formControls%}

</form>
EOF;

    public function getTemplate()
    {
        return $this->template;
    }

    public function compile()
    {   
        $this->setProperty('formId', 'form-id');
        $this->setProperty('formClass', 'form-class');

        $this->setProperty('saveFormButtonLabel', 'Save');
        $this->setProperty('cancelFormButtonLabel', 'Cancel');

        $this->addChildTemplate('formControls', $this->templateFactory->get('form_controls'));

        return $this;
    }
}
