<?php

use Enjoying\Core\DependencyInjection\ServiceDefinition;

return [

    'database_connection_factory' => new ServiceDefinition('Enjoying\Core\Factory\DatabaseConnectionFactory'),

    'template_factory' => new ServiceDefinition('Enjoying\Core\Factory\BaseTemplateFactory'),
    'view_factory' => new ServiceDefinition('Enjoying\Core\Factory\ViewFactory')
];
