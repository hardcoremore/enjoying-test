<?php

namespace Enjoying\GuestBook\Controller;

use Enjoying\Core\BaseController;

class GuestBookController extends BaseController
{
    public function homePage()
    {
        $userData = $this->getModel('user')->getUserData();

        $template = $this->getTemplate('guest_book_home_page');
        $template->setHeaderData($userData);
        $template->setProperty('createGuestBookUrl', '/guestbook/create');

        $template->setGuestBookData($this->getModel('guest_book')->readAll());

        return $this->getView('html')->setTemplate($template);
    }

    public function create()
    {
        $userData = $this->getModel('user')->getUserData();

        $template = $this->getTemplate('guest_book_form_page');
        $template->setHeaderData($userData);
        $template->setSubmitUrl('/guestbook/create');
        $template->setCancelButtonUrl('/');

        $guestBookData = $this->getModel('guest_book')->readAll();

        return $this->getView('html')->setTemplate($template);
    }

    public function get($id)
    {
        $userData = $this->getModel('user')->getUserData();

        $template = $this->getTemplate('guest_book_home_page');
        $template->setHeaderData($userData);
        $template->setProperty('createGuestBookUrl', '/guestbook/create');

        $selectedGuestBook = $this->getModel('guestBook')->readAll();
        $guestBookData = $this->getModel('guestBook')->readAll();

        return $this->getView('html')->setTemplate($template);
    }
	
	public function fetchAllGuestBooksAjax($entryId)
    {
        return $this->getView('json')->setViewData(
            $this->getModel('guest_book')->getNodeChildren($entryId)
        );
	}
}
