<?php

namespace Enjoying\GuestBook\Factory;

use Enjoying\Core\Factory\BaseTableGatewayFactory;

use Enjoying\GuestBook\Database\GuestBookTable;

class GuestBookTableGatewayFactory extends BaseTableGatewayFactory
{
    public function createGuestBookTable()
    {
        $connection = $this->createDatabaseConnection();

        $table = new GuestBookTable();
        $table->setTableName('guest_book');
        $table->setIdColumnName('id');
        $table->setConnection($connection);

        return $table;
    }
}
