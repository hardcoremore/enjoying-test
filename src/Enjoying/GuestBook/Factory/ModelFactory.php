<?php

namespace Enjoying\GuestBook\Factory;

use Enjoying\Core\Factory\BaseCamelCaseFactory;

use Enjoying\GuestBook\Factory\GuestBookTableGatewayFactory;

use Enjoying\GuestBook\Model\UserModel;
use Enjoying\GuestBook\Model\GuestBookModel;

class ModelFactory extends BaseCamelCaseFactory
{
    private $tableGatewayFactory;

    public function __construct()
    {
        $this->methodPostfix = 'Model';
    }

    public function setTableGatewayFactory(GuestBookTableGatewayFactory $factory)
    {
        $this->tableGatewayFactory = $factory;
    }

    public function getTableGatewayFactory()
    {
        return $this->tableGatewayFactory;
    }

    public function createUserModel()
    {
        return new UserModel();
    }

    public function createGuestBookModel()
    {
        $table = $this->tableGatewayFactory->createGuestBookTable();

        $model = new GuestBookModel();
        $model->setTable($table);

        return $model;
    }
}