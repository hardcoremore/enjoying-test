<?php

namespace Enjoying\GuestBook\Factory;

use Enjoying\Core\Factory\BaseTemplateFactory;

use Enjoying\GuestBook\Template\AppMainTemplate;

use Enjoying\GuestBook\Template\PageHeaderTemplate;
use Enjoying\GuestBook\Template\PageFooterTemplate;
use Enjoying\GuestBook\Template\AppMainLayoutTemplate;
use Enjoying\GuestBook\Template\BasePageTemplate;

use Enjoying\GuestBook\Template\GuestBookHomePageTemplate;
use Enjoying\GuestBook\Template\GuestBookFormPageTemplate;
use Enjoying\GuestBook\Template\GuestBookFormTemplate;
use Enjoying\GuestBook\Template\GuestBookRowTemplate;

class GuestBookTemplateFactory extends BaseTemplateFactory
{
    public function createAppMainTemplate()
    {
        return new AppMainTemplate($this);
    }

    public function createPageHeaderTemplate()
    {
        return new PageHeaderTemplate($this);
    }

    public function createPageFooterTemplate()
    {
        return new PageFooterTemplate($this);
    }

    public function createBasePageTemplate()
    {
        return new BasePageTemplate($this);
    }

    public function createAppMainLayoutTemplate()
    {
        return new AppMainLayoutTemplate($this);
    }


    public function createGuestBookHomePageTemplate()
    {
        return new GuestBookHomePageTemplate($this);
    }

    public function createGuestBookFormTemplate()
    {
        return new GuestBookFormTemplate($this);   
    }

    public function createGuestBookFormPageTemplate()
    {
        return new GuestBookFormPageTemplate($this);   
    }

    public function createGuestBookRowTemplate()
    {
        return new GuestBookRowTemplate($this);   
    }
}