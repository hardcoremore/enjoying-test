<?php

use Enjoying\Core\Router\RouteDefinition;

CONST GUEST_BOOK_ROUTE = 'guestbook';
CONST GUEST_BOOK_CONTROLLER = 'Enjoying\GuestBook\Controller\GuestBookController';

return [

    RouteDefinition::instance([RouteDefinition::GET, RouteDefinition::POST])->addSlug(GUEST_BOOK_ROUTE)
                                                   ->addSlug('create')
                                                   ->setControllerClass(GUEST_BOOK_CONTROLLER)
                                                   ->setControllerMethod('create'),

    RouteDefinition::instance(RouteDefinition::GET)->addSlug(GUEST_BOOK_ROUTE)
                                                   ->addSlug('get')
                                                   ->addSlug('id', false, RouteDefinition::DIGIT_ONLY)
                                                   ->setControllerClass(GUEST_BOOK_CONTROLLER)
                                                   ->setControllerMethod('get'),

    RouteDefinition::instance(RouteDefinition::DELETE)->addSlug(GUEST_BOOK_ROUTE)
                                                      ->addSlug('delete')
                                                      ->addSlug('id', false, RouteDefinition::DIGIT_ONLY)
                                                      ->setControllerClass(GUEST_BOOK_CONTROLLER)
                                                      ->setControllerMethod('delete'),


    RouteDefinition::instance(RouteDefinition::GET)->addSlug(GUEST_BOOK_ROUTE)
                                                   ->addSlug('node')
                                                   ->addSlug('nodeId', false, RouteDefinition::DIGIT_ONLY)
                                                   ->setControllerClass(GUEST_BOOK_CONTROLLER)
                                                   ->setControllerMethod('fetchAjaxTreeNode')
];
