<?php

use Enjoying\Core\DependencyInjection\ServiceDefinition;

return [

    'table_gateway_factory' => new ServiceDefinition(
        'Enjoying\GuestBook\Factory\GuestBookTableGatewayFactory',
        null,
        [
            'setDependencyContainer' => '@dependency_container'
        ]
    ),

    'model_factory' => new ServiceDefinition(
        'Enjoying\GuestBook\Factory\ModelFactory',
        null,
        [
            'setTableGatewayFactory' => '@table_gateway_factory'
        ]
    ),

    'template_factory' => new ServiceDefinition('Enjoying\GuestBook\Factory\GuestBookTemplateFactory')
];
