<?php

namespace Enjoying\GuestBook\Model;

use Enjoying\Core\BaseModel;

class UserModel extends BaseModel
{
    public function getUserData()
    {
        return [
            'firstName' => 'Časlav',
            'lastName' => 'Šabani',
            'email' => 'caslav.sabani@gmail.com'
        ];
    }
}
