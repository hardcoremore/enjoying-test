<?php

namespace Enjoying\GuestBook\Template;

use Enjoying\Core\Template\BaseHtmlTemplate;
use Enjoying\GuestBook\TemplateTraits\HeaderTemplateData;

class GuestBookFormPageTemplate extends BaseHtmlTemplate
{
    use HeaderTemplateData;

    private $template = <<<EOF
    <h2>Craete new Guest Book</h2>
    <div>{{errorMessage}}</div>

    <div class="form-container">
        {%guestBookForm%}
    </div>
EOF;

    public function getTemplate()
    {
        return $this->template;
    }

    private $cancelButtonUrl;

    private $errorMessage = '';

    private $submitUrl = '';

    public function setCancelButtonUrl($url)
    {
        $this->cancelButtonUrl = $url;
    }

    public function setErrorMessage($message)
    {
        $this->errorMessage = $message;
    }

    public function setSubmitUrl($url)
    {
        $this->submitUrl = $url;
    }
    

    public function compile()
    {
        $this->setProperty('errorMessage', $this->errorMessage);

        $formTemplate = $this->templateFactory->get('guest_book_form');
        $formTemplate->setProperty('actionUrl', $this->submitUrl);

        $formInputs = [];

        $input = $this->templateFactory->get('text_input_form_item');
        $input->setProperty('label', 'Name');
        $input->setProperty('placeHolder', 'Name');
        $input->setProperty('name', 'name');
        $input->compile();

        $formInputs[] = $input;

        $input = $this->templateFactory->get('text_input_form_item');
        $input->setProperty('label', 'Title');
        $input->setProperty('placeHolder', 'Title');
        $input->setProperty('name', 'title');

        $formInputs[] = $input;

        $input = $this->templateFactory->get('text_area_input_form_item');
        $input->setProperty('label', 'Comment');
        $input->setProperty('placeHolder', 'Comment');
        $input->setProperty('name', 'comment');

        $formInputs[] = $input;

        $input = $this->templateFactory->get('text_input_form_item');
        $input->setProperty('label', 'Email');
        $input->setProperty('placeHolder', 'Email');
        $input->setProperty('name', 'email');

        $formInputs[] = $input;
        
        $formTemplate->addChildTemplate('formInputs', $formInputs);

        $this->addChildTemplate('guestBookForm', $formTemplate);

        // template needds to be compiled before getting child templates
        $formTemplate->compile();
        $formTemplate->getChildTemplate('formControls')->setProperty('cancelFormButtonUrl', $this->cancelButtonUrl);
        
        $pageTemplate = $this->templateFactory->get('base_page');
        $pageTemplate->setHeaderData($this->getHeaderData());

        $main = $pageTemplate->compile();
        $main->addChildTemplate('pageContent', $this);

        return $main;
    }
}