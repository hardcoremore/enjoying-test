<?php

namespace Enjoying\GuestBook\Template;

use Enjoying\Core\Template\FormTemplate;

class GuestBookFormTemplate extends FormTemplate
{
    public function compile()
    {
        parent::compile();

        return $this;
    }
}
