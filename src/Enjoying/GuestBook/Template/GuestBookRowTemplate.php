<?php

namespace Enjoying\GuestBook\Template;

use Enjoying\Core\Template\BaseHtmlTemplate;

class GuestBookRowTemplate extends BaseHtmlTemplate
{
    private $template = <<<EOF
    <tr>
        <td>{{name}}</td>
        <td>{{title}}</td>
        <td>{{comment}}</td>
        <td>{{title}}</td>
        <td>
            <a href="guestbook/delete/{{id}}"></a>
            <a href="guestbook/edit/{{id}}"></a>
        </td>
    </tr>
EOF;

    public function getTemplate()
    {
        return $this->template;
    }
}