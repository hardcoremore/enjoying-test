<?php

namespace Enjoying\GuestBook\Template;

use Enjoying\Core\Template\BaseHtmlTemplate;

class AppMainTemplate extends BaseHtmlTemplate
{
    private $template = <<<EOF
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Enjoying Guest Book</title>
</head>

<body class="test-123">
{%mainLayout%}
</body>

</html>
EOF;

    public function getTemplate()
    {
        return $this->template;
    }
}