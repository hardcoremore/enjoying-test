<?php

namespace Enjoying\GuestBook\TemplateTraits;

trait HeaderTemplateData
{
    protected $headerData;

    public function setHeaderData(array $data)
    {
        $this->headerData = $data;
    }

    public function getHeaderData()
    {
        return $this->headerData;
    }
}